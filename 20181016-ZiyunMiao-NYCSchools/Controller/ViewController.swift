//
//  ViewController.swift
//  20181016-ZiyunMiao-NYCSchools
//
//  Created by ZIYUN MIAO on 10/16/18.
//  Copyright © 2018 ZIYUN MIAO. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var myActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "NYC-Schools"
        
        measureTime {
            loadlist()
        }
        
        readyToRefresh()
    }
    
    // MARK: - Load Data
    fileprivate func loadlist() {
        
        let task = URLSession.shared.dataTask(with: NYCUrl!) {
            (data, response, error) in
            
            if error != nil {
                print("Error")
            } else {
                do {
                    if let content = data {
                        let myJson = try JSONSerialization.jsonObject(with: content, options: .mutableContainers)
                        if let results = myJson as? [[String : Any]] {
                            for myData in results {
                                let schoolInfo = schoolData(myData: myData)
                                tableViewDataSource.append(schoolInfo)
                            }
                            tableViewDataSource = tableViewDataSource.sorted(){(element1: schoolData, element2: schoolData) -> Bool in
                                return element1.school_name < element2.school_name
                            }
                            DispatchQueue.main.async {
                                self.downloading(true)
                                searchDataSource = tableViewDataSource
                                self.myTableView.reloadData()
                            }
                        }
                    }
                } catch let error as Error? {
                    print("Read File Error", error!)
                }
            }
        }
        task.resume()
    }

// MARK: - Helper Methods
    fileprivate func downloading(_ isDownloading: Bool) {
        if isDownloading {
            self.myActivityIndicator.startAnimating()
        }
        else {
            self.myActivityIndicator.stopAnimating()
        }
        
        self.myActivityIndicator.isHidden = !isDownloading
        self.myTableView.alpha = isDownloading ? 0.2 : 1.0
    }
    
    @objc fileprivate func refreshing() {
        myTableView.refreshControl?.beginRefreshing()
        myTableView.reloadData()
        myTableView.refreshControl?.endRefreshing()
        print("Refreshed....")
    }
    
    fileprivate func readyToRefresh() {
        myTableView.refreshControl = UIRefreshControl()
        myTableView.refreshControl?.attributedTitle = NSAttributedString(string:"Refreshing")
        myTableView.refreshControl?.addTarget(self, action: #selector(refreshing), for: .valueChanged)
    }
    
// MARK: Pass data
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "segueCell")
        {
            if let indexPath = myTableView.indexPathForSelectedRow
            {
                if let detailVC = segue.destination as? ScoreDetailViewController
                {
                    detailVC.schoolDatas = searchDataSource[indexPath.row]
                }
            }
        }
    }
}

// MARK: - Table View DataSource and Delegate
extension ViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchDataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tableView.dequeueReusableCell(withIdentifier: "reuseCell", for: indexPath)
        let mySchooleNameLabel = myCell.viewWithTag(11) as! UILabel
        
        mySchooleNameLabel.text = searchDataSource[indexPath.row].school_name
        return myCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.downloading(false)
        cell.backgroundColor = UIColor.clear
        
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
        UIView.animate(withDuration: 0.25, animations: {
            cell.layer.transform = CATransform3DMakeTranslation(1,1,1)
        })
    }
}

// MARK: - Search Bar Delegate
extension ViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != "" {
            searchDataSource = tableViewDataSource.filter(){
                $0.school_name.lowercased().hasPrefix(searchText.lowercased())
            }
        } else {
            searchDataSource = tableViewDataSource
        }
        myTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.becomeFirstResponder()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        myTableView.reloadData()
    }
}
