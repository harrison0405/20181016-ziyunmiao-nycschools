//
//  ScoreDetailViewController.swift
//  20181016-ZiyunMiao-NYCSchools
//
//  Created by ZIYUN MIAO on 10/17/18.
//  Copyright © 2018 ZIYUN MIAO. All rights reserved.
//

import UIKit
import Charts

class ScoreDetailViewController: UIViewController {
    
    @IBOutlet weak var pieChartView: PieChartView!
    
    internal var schoolDatas = schoolData()
    private var numsOfDataEntriesd = [PieChartDataEntry]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ReadingScore = PieChartDataEntry(value: Double(schoolDatas.sat_critical_reading_avg_score) ?? 0.0)
        let MathScore = PieChartDataEntry(value: Double(schoolDatas.sat_math_avg_score) ?? 0.0)
        let WritingScore = PieChartDataEntry(value: Double(schoolDatas.sat_writing_avg_score) ?? 0.0)
        
        pieChartView.chartDescription?.text = schoolDatas.school_name
        ReadingScore.label = "Reading"
        MathScore.label = "Math"
        WritingScore.label = "Writing"
        
        numsOfDataEntriesd = [ReadingScore,MathScore,WritingScore]
        
        updateChartData()
    }
    
    fileprivate func updateChartData() {
        
        let chartDataSet = PieChartDataSet(values: numsOfDataEntriesd, label: nil)
        let chartData = PieChartData(dataSet: chartDataSet)
        chartDataSet.colors = iOSColors
        pieChartView.data = chartData
    }
}


