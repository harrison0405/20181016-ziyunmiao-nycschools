//
//  GlobalFile.swift
//  20181016-ZiyunMiao-NYCSchools
//
//  Created by ZIYUN MIAO on 10/17/18.
//  Copyright © 2018 ZIYUN MIAO. All rights reserved.
//

import Foundation
import UIKit

internal let NYCUrl = URL(string: "https://data.cityofnewyork.us/resource/734v-jeq5.json")

internal var tableViewDataSource = [schoolData]()
internal var searchDataSource = [schoolData]()

public func measureTime(execute: () -> Void) {
    let startTime = CACurrentMediaTime()
    execute()
    let endTime = CACurrentMediaTime()
    print("Loading Time - \(endTime - startTime)")
}

public let iOSColors: [UIColor] = [UIColor.green,UIColor.blue,UIColor.red]


