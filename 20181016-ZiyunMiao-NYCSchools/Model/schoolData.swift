//
//  schoolData.swift
//  20181016-ZiyunMiao-NYCSchools
//
//  Created by ZIYUN MIAO on 10/16/18.
//  Copyright © 2018 ZIYUN MIAO. All rights reserved.
//

import Foundation

struct schoolData {
    
    var num_of_sat_test_takers : String
    var sat_critical_reading_avg_score : String
    var sat_math_avg_score : String
    var sat_writing_avg_score : String
    var school_name : String
    
    init(myData:[String : Any]) {
        
        num_of_sat_test_takers = myData["num_of_sat_test_takers"] as? String ?? "No Test Taker"
        sat_critical_reading_avg_score = myData["sat_critical_reading_avg_score"] as? String ?? "No Reading Score"
        sat_math_avg_score = myData["sat_math_avg_score"] as? String ?? "No Math Score"
        sat_writing_avg_score = myData["sat_writing_avg_score"] as? String ?? "No Writing Score"
        school_name = myData["school_name"] as? String ?? "No School Name"
    }
    
    init() {
        self.num_of_sat_test_takers = ""
        self.sat_critical_reading_avg_score = ""
        self.sat_math_avg_score = ""
        self.sat_writing_avg_score = ""
        self.school_name = ""
    }
}
